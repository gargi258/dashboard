import config from '../../config'; // TODO what happen when file not exist?

const uuidv4 = require('uuid/v4');

config.services
    .forEach(function (service) {
        service.envs.forEach(function (env) {
            env.hosts.forEach(function (host) {
                host.id = uuidv4();
            })
        })
    });

export default config;