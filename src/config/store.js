import Vue from 'vue';
import Vuex from "vuex";
import config from "./config";

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        config: config,
        now: new Date(),
        nodes: [],
    },
    getters: {
        getNodeState: (state) => (id) => {
            const node = state.nodes.find((n) => id === n.id);

            return node !== undefined
                ? node.state
                : {};
        },
        nodeCheckingFailed: (state) => (id) => {
            const node = state.nodes.find((n) => id === n.id);

            return node !== undefined
                ? node.checkingFailed
                : false;
        },
        getServiceByName: (state) => (name) => {
            return state.config.services
                .find((s) => s.name === name);
        }
    },
    mutations: {
        // TODO add const for available mutations
        registerNode(state, payload) {
            state.nodes.push({
                id: payload.id,
                state: {},
                checkingFailed: false,
            });
        },
        updateNodeState(state, payload) {
            let node = state.nodes.find((n) => n.id === payload.id);
            node.checkingFailed = false;
            node.state = payload.state;
        },
        checkingFailed(state, payload) {
            let node = state.nodes.find((n) => n.id === payload.id);
            node.checkingFailed = true;
            node.state = {
                reason: payload.reason,
            };
        },
    },
});

export default store;
