import Vue from 'vue';
import Vuec from "vue-container";
import Factory from "../service/Checker/Factory";
import config from "./config";

Vue.use(Vuec);

Vue.$ioc.register('Factory', new Factory(config));
