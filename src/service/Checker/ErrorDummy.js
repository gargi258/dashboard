export default class ErrorDummy {

    constructor() {
    }

    getStatus() {
        return new Promise(function (resolve) {
            resolve({
                "details": {
                    "Some_0 Service API": {
                        "result": "success",
                        "message": "",
                        "data": "Message of: Some_0 Service API"
                    },
                    "Some_1 Service API": {
                        "result": "failure",
                        "message": "",
                        "data": "Message of: Some_1 Service API"
                    },
                    "Some_2 Service API": {
                        "result": "warning",
                        "message": "",
                        "data": "Message of: Some_2 Service API"
                    },
                    "Some_3 Service API": {
                        "result": "unknown",
                        "message": "",
                        "data": "Message of: Some_3 Service API"
                    },
                    "Some_4 Service API": {
                        "result": "skip",
                        "message": "",
                        "data": "Message of: Some_4 Service API"
                    },
                    "Connection to database \"doctrine.connection.orm_default\"": {
                        "result": "success",
                        "message": null,
                        "data": {
                            "username": "user",
                            "host": "database.pl",
                            "port": 3306,
                            "database": "db"
                        }
                    },
                    "Build version check": {
                        "result": "success",
                        "message": "Installed version: 1.0.0_111\n",
                        "data": null
                    }
                },
                "success": 3,
                "warning": 1,
                "failure": 1,
                "skip": 1,
                "unknown": 1,
                "passed": false
            });
        });
    }
}
