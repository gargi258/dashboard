export default class Delayed {
    delay;
    checker;

    constructor(delay, checker) {
        this.delay = delay;
        this.checker = checker;
    }

    getStatus() {

        return new Promise((resolve) => {
            setTimeout(
                () => resolve(this.checker.getStatus()),
                this.delay
            );
        })
    }
}
