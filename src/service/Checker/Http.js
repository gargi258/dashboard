import axios from 'axios';

export default class Http {
    url;
    httpClient;

    constructor(url, timeout) {
        this.url = url;
        this.httpClient = axios.create({
            timeout: timeout,
        });
    }

    getStatus() {
        return new Promise((resolve, reject) => {
            this.httpClient
                .get(this.url)
                .then(response => resolve(response.data))
                .catch(reason => {
                    const response = reason.response;

                    if (undefined !== response && response.status >= 400) {
                        reject(response.statusText);
                    }

                    if (undefined !== reason.message) {
                        reject(reason.message);
                    }

                    reject("Unknown error");
                });
        });
    }
}
