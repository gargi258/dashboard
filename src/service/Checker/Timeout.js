export default class Timeout {
    timeout;
    checker;

    constructor(timeout, checker) {
        this.timeout = timeout;
        this.checker = checker;
    }

    getStatus() {
        return new Promise((resolve, reject) => {
            this.checker.getStatus()
                .then(response => resolve(response))
                .catch(reason => reject(reason));

            setTimeout(
                () => reject('Timeout after ' + this.timeout + ' ms'),
                this.timeout
            );
        })
    }
}
