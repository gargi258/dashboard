export default class Random {
    static positiveStatuses = ['success', 'warning', 'skip', 'unknown',];
    static allStatuses = ['success', 'warning', 'failure', 'skip', 'unknown',];

    constructor() {
    }

    getStatus() {
        return new Promise((resolve) => {
            let status = {
                "details": {
                    "Connection to database \"doctrine.connection.orm_default\"": {
                        "result": "success",
                        "message": null,
                        "data": {
                            "username": "user",
                            "host": "database.pl",
                            "port": 3306,
                            "database": "db"
                        }
                    },
                    "Build version check": {
                        "result": "success",
                        "message": "Installed version: 1.0.0_111\n",
                        "data": null
                    }
                },
                "success": 2,
                "warning": 0,
                "failure": 0,
                "skip": 0,
                "unknown": 0,
                "passed": !Random.getRandomInt(0,1),
            };

            if (status.passed) {
                Random.randomCheckState(
                    status,
                    Random.getRandomInt(4, 10),
                    Random.positiveStatuses
                );
            } else {
                Random.randomCheckState(
                    status,
                    Random.getRandomInt(4, 18),
                    Random.allStatuses
                );

                status.details['Some_Always_Failing Service API'] = {
                    "result": "failure",
                    "message": "",
                    "data": "Message of: Some_' + i + ' Service API"
                };
            }

            resolve(status);
        });
    }

    static getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    static randomCheckState(status, quantity, statuses) {
        for (let i = 0; i < quantity; i++) {
            const statusName = Random.randStatus(statuses);

            status.details['Some_' + i + ' Service API'] = {
                "result": statusName,
                "message": "",
                "data": "Message of: Some_' + i + ' Service API"
            };

            status[statusName]++;

            if (statusName === 'failure') {
                status.passed = false;
            }
        }
    }

    static randStatus(statuses) {
        return statuses[Math.floor(Math.random() * statuses.length)];
    }
}
