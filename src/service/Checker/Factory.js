import SuccessDummy from "./SuccessDummy";
import Http from "./Http";
import Random from "./Random";
import Delayed from "./Delayed";
import Timeout from "./Timeout";
import ErrorDummy from "./ErrorDummy";

export default class Factory {
    config;

    constructor(config) {
        this.config = config;
    }

    getCheckerBy(serviceConfig) {
        let checker = undefined;

        if (serviceConfig.checkInterface === "http") {
            checker = new Http(
                serviceConfig.url,
                serviceConfig.hasOwnProperty('timeout')
                    ? serviceConfig.timeout
                    : 3000
            );
        }

        if (serviceConfig.checkInterface === "random") {
            checker = new Random();
        }

        if (serviceConfig.checkInterface === "delayed") {
            let innerCheckerConfig = serviceConfig;
            innerCheckerConfig.checkInterface = innerCheckerConfig.innerChecker.checkInterface;

            checker = new Delayed(
                serviceConfig.delay,
                this.getCheckerBy(innerCheckerConfig)
            );
        }

        if (serviceConfig.checkInterface === "success-dummy") {
            checker = new SuccessDummy();
        }

        if (serviceConfig.checkInterface === "error-dummy") {
            checker = new ErrorDummy();
        }

        if (undefined !== checker) {
            return new Timeout(
                this.config.hasOwnProperty('timeout')
                    ? this.config.timeout
                    : 3000,
                checker
            );
        }

        throw 'Unknown checkInterface: ' + serviceConfig.checkInterface;
    }
}
